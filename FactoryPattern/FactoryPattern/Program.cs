﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {

            Person _person = new Person() { Id = 19, Name = "Charlie" };
            Car _car = new Car() { Id = 19,Brand = "Ford" };


            PersonFactory _personFactory = new PersonFactory();
            CarFactory _carFactory = new CarFactory();


            Console.WriteLine("Numbers of persons: {0}", _personFactory.GetItems().ToString());
            Console.WriteLine("Numbers of cars: {0}", _personFactory.GetItems().ToString());
            Console.ReadKey();

            Console.WriteLine("----Adding New Person And Car----");

            _personFactory.Add(_person);
            _carFactory.Add(_car);

            Console.WriteLine("----Items Added----");
            Console.WriteLine("Numbers of persons: {0}", _personFactory.GetItems().ToString());
            Console.WriteLine("Numbers of cars: {0}", _personFactory.GetItems().ToString());

            Console.ReadKey();

        }
    }
}
