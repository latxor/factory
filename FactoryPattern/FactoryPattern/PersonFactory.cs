﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    public class PersonFactory : IFactory<Person>
    {
        public static List<Person> Persons = new List<Person>()
        {
            new Person() { Id=1, Name = "Person 1"  },
            new Person() { Id=2, Name = "Person 2"  },
            new Person() { Id=3, Name = "Person 3"  }

        };

        public bool Add(Person item)
        {
            if (item!=null)
            {
                Persons.Add(item);
                return true;
            }
            return false;
        }

        public bool Delete(Person item)
        {
            return Persons.Remove(item);
        }

        public int GetItems()
        {
            return Persons.Count;
        }
    }
}
