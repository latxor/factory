﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    public interface IFactory<T> where T: class
    {
        bool Add(T item);
        bool Delete(T item);
        int GetItems();
    }
}
