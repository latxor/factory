﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    public class CarFactory: IFactory<Car>
    {
        public static List<Car> Cars = new List<Car>()
        {
            new Car() { Id=1, Brand = "Brand 1"  },
            new Car() { Id=2, Brand = "Brand 2"  },            

        };

        public bool Add(Car item)
        {
            if (item != null)
            {
                Cars.Add(item);
                return true;
            }
            return false;
        }

        public bool Delete(Car item)
        {
            return Cars.Remove(item);
        }

        public int GetItems()
        {
            return Cars.Count;
        }
    }
}
